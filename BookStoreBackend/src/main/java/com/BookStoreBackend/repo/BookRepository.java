package com.BookStoreBackend.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.BookStoreBackend.model.Book;

public interface BookRepository extends JpaRepository<Book, Long> {

}
