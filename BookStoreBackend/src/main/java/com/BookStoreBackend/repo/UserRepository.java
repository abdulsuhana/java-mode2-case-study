package com.BookStoreBackend.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.BookStoreBackend.model.User;

public interface UserRepository extends JpaRepository<User,Long> {

}
